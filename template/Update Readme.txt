Patch - 1.1.2(Bug Fix Only)

The Left Sidemenu was not displaying correctly in Safari browsers and on Android based devices. The problem
was occuring because of a z-index issue with the primary containers(#content_wrapper, #sidebar_left). 

Styles removed in patch:

- #content_wrapper { z-index: 1025; }

- #content_wrapper:after (entire style)

- #sidebar_left { z-index: 1024; }

- #sidebar_left:after (entire style)


