<div class="panel-bgwide">
        <div class="panel panelwide panel-overflow">
          <div class="panel-heading"> <span class="panel-title"> <span class="fa fa-legal text-purple2"></span> Legal </span> </div>
          <div class="panel-menu">
            <a class="btn btn-sm bg-purple2" href="javascript:history.go(-1);" ><i class="fa fa-arrow-left"></i> Go back</a>
            <a class="btn btn-sm btn-default" href="#"><i class="fa fa-print"></i> Printer friendly version</a>
          </div>
            <div class="row table-layout">
              <div class="col-sm-3 panel-sidemenu border-right">
              <ul class="nav">
                <li class="nav-title"><a href="#">Privacy statement</a></li>
                <li class="nav-title"><a href="#">Terms and conditions</a></li>
                <li class="nav-title"><a href="#">Account Billing</a></li>
                <li class="nav-title"><a href="#">Security certificates</a></li>
              </ul>
              </div>
              <div class="col-sm-9">
                <div class="panel-group accordion mt25" id="accordion1">
                    <div class="panel" style="width:100%;">
                      <div class="panel-heading"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accord1_1">
                        <div class="accordion-toggle-icon pull-left"> <i class="fa fa-minus-square-o text-light4"></i> <i class="fa fa-plus-square-o text-purple2"></i> </div>
                        1. Oblicity App general terms </a> </div>
                      <div id="accord1_1" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consequat aliquam odio quis hendrerit. Morbi elit lectus, congue sit amet pretium consequat, tristique quis sapien. Duis posuere ex neque, id consequat justo euismod sed. Maecenas laoreet massa vitae commodo vehicula. Vivamus vel ex convallis, pharetra ipsum ac, suscipit enim. Fusce dapibus dolor vel velit sodales tristique. Pellentesque orci dolor, dictum ac felis quis, efficitur vulputate magna. Cras bibendum cursus elit ac pharetra. Vestibulum justo neque, tincidunt eu dictum vel, cursus vel ante. Praesent viverra ex sed odio lacinia ultrices. Quisque in cursus nisi. Fusce ut nisi ut urna dignissim egestas. Nam ex tellus, maximus quis dapibus ac, tincidunt non odio. Proin augue augue, mollis nec est vitae, porttitor sodales sapien.</div>
                      </div>
                    </div>
                    <div class="panel" style="width:100%;">
                      <div class="panel-heading"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accord1_2">
                        <div class="accordion-toggle-icon pull-left"> <i class="fa fa-minus-square-o text-light4"></i> <i class="fa fa-plus-square-o text-purple2"></i> </div>
                        1.1 Provisions </a> </div>
                      <div id="accord1_2" class="panel-collapse collapse">
                        <div class="panel-body">Nam ex tellus, maximus quis dapibus ac, tincidunt non odio. Proin augue augue, mollis nec est vitae, porttitor sodales sapien. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consequat aliquam odio quis hendrerit. Morbi elit lectus, laoreet massa vitae commodo vehicula. Vivamus vel ex convallis, sodales tristique. Pellentesque orci dolor, dictum ac felis quis, efficitur vulputate magna. Cras bibendum cursus elit ac pharetra. Vestibulum justo neque, tincidunt eu dictum vel, cursus vel ante. Praesent viverra ex sed odio lacinia ultrices. Quisque in cursus nisi. Fusce ut nisi ut urna dignissim egestas.</div>
                      </div>
                    </div>
                    <div class="panel" style="width:100%;">
                      <div class="panel-heading"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accord1_3">
                        <div class="accordion-toggle-icon pull-left"> <i class="fa fa-minus-square-o text-light4"></i> <i class="fa fa-plus-square-o text-purple2"></i> </div>
                        1.2 Account security </a> </div>
                      <div id="accord1_3" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">Maecenas vestibulum accumsan turpis a euismod. Mauris id elit orci. Curabitur malesuada dolor nec mi malesuada, in facilisis sapien scelerisque. Quisque volutpat erat a ipsum cursus laoreet. Duis a turpis eu urna ornare cursus at sit amet enim. Nulla viverra tellus at neque pretium aliquam. Sed pulvinar, ipsum non sagittis porttitor, neque ex pulvinar erat, ac sodales metus risus id lectus. Aliquam non ipsum ut magna interdum consectetur vulputate id massa. Maecenas sed blandit velit. Aenean laoreet sollicitudin porttitor. </div>
                      </div>
                    </div>
                    <div class="panel" style="width:100%;">
                      <div class="panel-heading"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accord1_4">
                        <div class="accordion-toggle-icon pull-left"> <i class="fa fa-minus-square-o text-light4"></i> <i class="fa fa-plus-square-o text-purple2"></i> </div>
                        2. Billing information </a> </div>
                      <div id="accord1_4" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">Aliquam eu neque justo. Nam sodales, libero vel semper maximus, justo mi bibendum magna, ut scelerisque mi mi ut tellus. Aenean ornare scelerisque nulla id scelerisque. Phasellus magna ante, hendrerit ut eros bibendum, fringilla tristique urna. Nulla dictum, dui non viverra porta, lorem elit tincidunt metus, vel viverra mauris velit eu mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam rhoncus turpis nisl, ac molestie purus venenatis ac. Aenean cursus nisl sed ligula dictum, et tincidunt leo porta. </div>
                      </div>
                    </div>
                    <div class="panel" style="width:100%;">
                      <div class="panel-heading"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accord1_5">
                        <div class="accordion-toggle-icon pull-left"> <i class="fa fa-minus-square-o text-light4"></i> <i class="fa fa-plus-square-o text-purple2"></i> </div>
                        2.1 Credit Card No-Storage policy </a> </div>
                      <div id="accord1_5" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">Nunc auctor elit turpis, eu pharetra velit accumsan id. Integer sodales eu arcu eget vulputate. Morbi fermentum ut metus at aliquam. Vestibulum mollis, lacus quis tristique pharetra, mauris purus suscipit ipsum, id tempus nunc massa vitae ante. </div>
                      </div>
                    </div>
                    <div class="panel" style="width:100%;">
                      <div class="panel-heading"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#accord1_6">
                        <div class="accordion-toggle-icon pull-left"> <i class="fa fa-minus-square-o text-light4"></i> <i class="fa fa-plus-square-o text-purple2"></i> </div>
                        3. Dispute mitigation </a> </div>
                      <div id="accord1_6" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">Integer in mi ac ante volutpat feugiat eu vel nisi. Curabitur est quam, posuere at nisl vitae, pharetra sollicitudin nunc. Etiam pellentesque, nisl vitae sodales fringilla, massa magna aliquet massa, et ullamcorper odio orci vel eros. Nunc eleifend ornare tempor. Nullam scelerisque felis ex, sit amet mattis neque porta in. Aliquam eu nisi ac ex mattis rutrum at vehicula nisi. Praesent ac vestibulum odio, at vulputate libero. </div>
                      </div>
                    </div>
                  </div>
              </div>
           </div>
          <div class="panel-footer text-center fs10">
               <?php include('footer_notes.php'); ?>
          </div>
        </div>
      </div>