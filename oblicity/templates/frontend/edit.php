<!DOCTYPE html>
<html>
<head>
<?php include('head_base.php');?>
</head>
<body>
<script> var boxtest = localStorage.getItem('boxed'); if (boxtest === 'true') {document.body.className+=' boxed-layout';} </script>
<?php
  $names = array("John Levemberg","Andrea Paoletti","Arnold J. Assoc.","Dr. Pablo Neruda","Mika Aalto", "Paavo Kulmala", "Lauri J&auml;rvi", "Sanna Lahti", "Kristina Lukkanen", "Mika Jokela", "Laura Takala", "Urmo Toivonen");
  $subjects = array("Nunc vel ultricies urna, in elementum lacus. Pellentesque quis libero tortor", "Pellentesque laoreet neque a mi tincidunt tincidunt", "Sed eros urna, scelerisque eu quam in, pharetra euismod nisl", "Sed sollicitudin efficitur mi, ut tincidunt felis facilisis ac", "Morbi sit amet dolor in est viverra gravida", "Nam vitae ante at quam placerat ornare", "In gravida elementum elit id pretium. Ut scelerisque libero augue");
  $filenames = array("doku_signed1.pdf","contract.pdf","nda.pdf","annexes.pdf","cv.pdf","certificates.pdf");
  $tags = array("work", "important", "delay", "business" , "online");
  $status = array("open","closed","canceled","updated", "renewed") ;
  $types = array("Contract for services", "Car selling","House rental", "Personal loan", "Rent to own", "Land sale","Construction", "Waiver of liability", "Last will and testament","Partnership agreement");
  $cnames = array("Contract", "Document", "Agreement_", "signed_doc_");
?>
<!-- Start: Header -->
<?PHP include('topbar.php'); ?>
<!-- End: Header --> 
<!-- Start: Main -->
<div id="main"> 
  <!-- Start: Sidebar -->
  <?php include('sidebar.php'); ?>
  <!-- End: Sidebar -->
  <!-- Start: Content -->
  <section id="content_wrapper">
    <div id="topbar">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active"><a href="#"><?php if(isset($_GET['id'])) { echo "Edit"; } else { echo "New Contract"; } ?></a></li>
          <li class="crumb-icon"><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
          <li class="crumb-link"><a href="main.php">Home</a></li>
          <li class="crumb-trail">Assets</li>
        </ol>
      </div>
    </div>
    <div id="content">
        <div class="row">
        <div class="col-md-12">
          <div class="panel invoice-panel">
            <div class="panel-heading panel-visible"> <span class="panel-title"> <span class="fa <?php if(isset($_GET['id'])) { echo "fa-edit"; } else { echo "fa-magic"; } ?>"></span> Procedures - <?php if(isset($_GET['id'])) { echo "Edit "; } else { echo "New "; } ?>Contract</span>
              <div class="panel-header-menu pull-right mr10">
                <button type="button" class="btn btn-xs btn-default btn-gradient mr5" onclick="window.location='edit.php'"> <i class="fa fa-plus-square pr5"></i>New Contract</button>
                <button type="button" class="btn btn-xs btn-default btn-gradient mr5" onclick="$('.xedit').editable('toggleDisabled')" > <i class="fa fa-edit pr5"></i>Toggle edit</button>
                <a href="javascript:window.print()" class="btn btn-xs btn-default btn-gradient mr5"> <i class="fa fa-print fs13"></i> </a>
                <div class="btn-group">
                  <button type="button" class="btn btn-xs btn-default btn-gradient dropdown-toggle" data-toggle="dropdown"><span class="glyphicons glyphicons-cogwheel"></span></button>
                  <ul class="dropdown-menu checkbox-persist pull-right text-left" role="menu">
                    <li><a>Configure </a></li>
                    <li><a>Track changes </a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="panel-body pn" id="invoice-item">
              <div class="col-md-12 va-m p30">
                <div class="row mb30">
                  <div class="col-md-4">
                    <div class="pull-left">
                              <h1 class="lh10"> <span class="xedit"><?php if(isset($_GET['id'])) { echo $types[rand(0,sizeof($types)-1)]; } else { echo "CONTRACT NAME"; } ?></span></h1>
                      <h5 class="mn lh30"> Dated: <span class="xedit">3/4/2015</span> </h5>
                      <h5 class="mn"> Status: <b class="text-<?php if(isset($_GET['id'])) { echo "blue"; } else { echo "green"; } ?>3"><?php if(isset($_GET['id'])) { echo "Saved"; } else { echo "Awaiting approval"; } ?></b> </h5>
                    </div>
                  </div>
                  <div class="col-md-4"> </div>
                  <div class="col-md-4">
                    <div class="pull-right text-right">
                      <h5> Person in charge: <b class="text-purple2 xedit"><?php if(isset($_GET['id'])) { echo $names[rand(0,sizeof($names)-1)]; } else { echo "Name Surname"; } ?></b> </h5>
                    </div>
                  </div>
                </div>
                <div class="row" id="invoice-info">
                  <div class="col-md-4">
                    <div class="panel panel-alt">
                      <div class="panel-heading"> <span class="panel-title"> <i class="fa fa-user"></i> Between the parties: </span>
                        <div class="panel-header-menu pull-right ">
                          <button type="button" class="btn btn-xs btn-success btn-gradient"> <i class="fa fa-plus-square pr5"></i>Add</button>
                        </div>

                      </div>
                      <div class="panel-body">
                        <address>
                        <strong><span class="xedit"><?php if(isset($_GET['id'])) { echo $names[rand(0,sizeof($names)-1)]; } else { echo "Name Surname"; } ?></span></strong><br>
                        <span class="xedit">Number Street, District</span><br>
                        <span class="xedit">City, COUNTRY, PT</span><br>
                        <abbr title="Phone">Phone:</abbr> <span class="xedit">(+)</span>
                        </address>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="panel panel-alt">
                      <div class="panel-heading"> <span class="panel-title"> <i class="fa fa-location-arrow"></i> Location of settlement:</span>
                      </div>
                      <div class="panel-body">
                        <address>
                        <strong><span class="xedit"><?php if(isset($_GET['id'])) { echo $names[rand(0,sizeof($names)-1)]; } else { echo "Name Surname"; } ?></span></strong><br>
                        <span class="xedit">Number Street, District</span><br>
                        <span class="xedit">City, COUNTRY, PT</span><br>
                        <abbr title="Phone">Phone:</abbr> <span class="xedit">(+)</span>
                        </address>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="panel panel-alt">
                      <div class="panel-heading"> <span class="panel-title"> <i class="fa fa-info"></i> Contract Details: </span>
                        <div class="panel-btns pull-right ml10"> </div>
                      </div>
                      <div class="panel-body">
                        <ul class="list-unstyled">
                          <li> <b>Contract #:</b> <?php echo rand(4553445,9588673); ?></li>
                          <li> <b>Inner Date:</b> <?php echo rand(1,31); ?>/<?php echo rand(0,12); ?>/<?php echo rand(14,15); ?></li>
                          <li> <b>Confirmation Date:</b> <?php echo rand(1,31); ?>/<?php echo rand(0,12); ?>/<?php echo rand(14,15); ?></li>
                          <li> <b>Terms:</b> -</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                 <?php if(!isset($_GET['id'])) { ?>
                <div class="row" >
                   <div class="col-md-12">
                    <form class="form-horizontal">
                            <div class="form-group">

                              <div class="col-md-12">
                              <label for="spinner1" class="control-label">Compose sections: </label>
                                <select id="multiselect1" style="display: none;">
                                  <option value="introduction" selected="">Introduction</option>
                                  <option value="mainsection">Main Section</option>
                                  <option value="agreements">Agreements</option>
                                  <option value="parties">Parties</option>
                                  <option value="liabilities">Liabilities</option>
                                  <option value="amounts">Amounts</option>
                                </select>
                                <div class="btn-group">
                                    <button type="button" class="multiselect dropdown-toggle btn btn-default" data-toggle="dropdown" title="Select section">Select section <b class="caret"></b></button>
                                    <ul class="multiselect-container dropdown-menu">
                                        <li class="">
                                            <a href="javascript:addsection(0);">
                                                <label class="radio">
                                                    Introduction
                                                </label>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="javascript:addsection(1);">
                                                <label class="radio">
                                                    Main Section
                                                </label>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="javascript:addsection(2);">
                                                <label class="radio">
                                                    Agreements
                                                </label>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="javascript:addsection(3);">
                                                <label class="radio">
                                                    Parties
                                                </label>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="javascript:addsection(4);">
                                                <label class="radio">
                                                    Liabilities
                                                </label>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="javascript:addsection(5);">
                                                <label class="radio">
                                                    Amounts
                                                </label>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                              </div>
                            </div>
                        </form>
                  </div>
                </div>
                <?php  } ?>
                <div class="row" >
                   <div class="col-md-12">
                    <div class="panel panel-alt">
                      <div class="panel-body pn">
                         <textarea name="editor1" id="editor1" name="editor1" rows="14">
                             <?php if(isset($_GET['id'])) {  ?>
                                <h2>Introduction</h2>
                                <p>This Contract, made this day of , __19 , between________________hereinafter referred to as Seller, whose address ___________________ hereinafter referred to as Purchaser, together with all tenements, hereditaments, improvements, and appurtenances, including any lighting and plumbing fixtures, shades, Venetian blinds, curtain rods, storm windows, storm doors, screens, awnings, TV antenna, now on the land, subject to any applicable building and use restrictions and to any easements affecting the land.</p>
                                <h2>Agreements</h2>
                                <p>To execute and deliver to Purchaser or his (_____________) assigns, upon payment in full of all sums owing hereon, less the amount then owing on any unpaid mortgage or mortgages, and the surrender of the duplicate of this contract, a good and sufficient warranty deed conveying title to the land, subject to abovementioned restrictions and easements and to any then unpaid mortgage or mortgages, but free from all other encumbrances, except such as may be herein set forth or shall have accrued or attached since the date hereof through the acts or omissions of persons other than Seller or his assigns.</p>
                                <h2>Main Section</h2>
                                <p>hat the full consideration for the sale of the land to Purchaser is: _____(&euro; ) euros, of which the sum of _____(&euro; ) euros has been paid to Seller prior to the delivery hereof, the receipt of which is hereby acknowledged, and the additional sum of _____(&euro;) euros, is to be paid to Seller, with interest on any part thereof at any time unpaid at the rate of per cent per annum while Purchaser is not in default, and at the rate of ________ per cent per annum.</p>
                                <h2>Liabilities</h2>
                                <ul>
                                	<li>To purchase the land and pay Seller the sum aforesaid, with interest thereon as above provided. In addition, _________________________________</li>
                                	<li>To use, maintain and occupy the land in accordance with any and all building and _______________________ and use restrictions applicable thereto.</li>
                                	<li>To keep the land in accordance with all police, sanitary or other regulations imposed by any governmental or _______________________ authority.</li>
                                	<li>To keep and maintain the land and the buildings and _____________________________ in as good condition as they are at the date hereof and not to commit waste, remove or demolish any improvements thereon, or otherwise diminish the value of Seller&#39;s security, without the written consent of Seller.</li>
                                </ul>
                                <h2>Amounts</h2>
                                <p>To pay monthly in addition to the monthly payment hereinbefore stipulated, the sum of ____________________(&euro; ) euros, which is an estimate of the monthly cost of taxes, special assessments, and insurance premiums for the land, which shall be credited by Seller on the unpaid principal balance owing on the contract. If Purchaser is not in default under the terms of this contract, Seller shall pay for Purchaser&#39;s account the taxes, special assessments and insurance premiums mentioned in Paragraph 2(e) above when due and before any penalty attaches, and submit receipts therefor to Purchaser upon demand. The amounts so paid shall be added to the principal balance of this contract. The amount of the estimated monthly payment, under this paragraph, may be adjusted.</p>
                             <?php } ?>
                          </textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row" id="invoice-footer">
                  <div class="col-md-12">
                    <div class="clearfix"></div>
                    <div class="invoice-buttons"> <a href="javascript:window.print()" class="btn btn-default mr10"><i class="fa fa-print pr5"></i> Print Contract</a>
                      <button class="btn bg-green3 bg-gradient" type="button"><i class="fa fa-credit-card"></i> Add signatures</button>
                      <button class="btn bg-purple2 bg-gradient" type="button" onclick="window.location='assets.php'"><i class="fa fa-floppy-o pr5"></i> Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End: Content --> 
  
  <!-- Start: Right Sidebar -->
    <?PHP include('sidebar_right.php'); ?>
  <!-- End: Right Sidebar --> 
</div>
<!-- End: Main --> 

<?php include('scripts_base.php');?>
<script type="text/javascript" src="vendor/editors/xeditable/js/bootstrap-editable.js"></script>
<script type="text/javascript" src="vendor/editors/ckeditor/ckeditor.js"></script>
<script type="text/javascript">


jQuery(document).ready(function () {
	 "use strict";
       // Init Xeditable Plugin
    $.fn.editable.defaults.mode = 'inline';
    $('.xedit').editable();

    <?php if(isset($_GET['id'])) { ?>

         $('.xedit').editable('toggleDisabled')

    <?php } ?>

    // Init Ckeditor. Replace Text area with Ckeditor
	 // and assign it a custom skin class
     CKEDITOR.replace( 'editor1',
	 {
	   on : { instanceReady : function ( evt )  {
			 $('#cke_editor1').addClass('fusionSkin');
		    }}
	 });

     // Turn off automatic editor initilization.
	 // Used to prevent conflictions with multiple text
	 // editors being displayed on the same page.
     CKEDITOR.disableAutoInline = true;

	 var prevColor = "";

	 // Theme Example - Ckeditor Colorpicker
	 $('.editor-ui-color').click(function () {
	 	var color = $(this).val();
		$('.fusionSkin .cke_top, .markItUpHeader, .note-toolbar').addClass(color).removeClass(prevColor);
		prevColor = color;
	 });

});

function addsection(v) {
  sections=["<h2>Introduction</h2><p>This Contract, made this  day of , __19 , between________________hereinafter referred to as Seller, whose address ___________________ hereinafter referred to as Purchaser, together with all tenements, hereditaments, improvements, and appurtenances, including any lighting and plumbing fixtures, shades, Venetian blinds, curtain rods, storm windows, storm doors, screens, awnings, TV antenna, now on the land, subject to any applicable building and use restrictions and to any easements affecting the land.</p>",
            "<h2>Main Section</h2><p>hat the full consideration for the sale of the land to Purchaser is: _____(&euro; ) euros, of which the sum of _____(&euro; ) euros has been paid to Seller prior to the delivery hereof, the receipt of which is hereby acknowledged, and the additional sum of _____(&euro;) euros, is to be paid to Seller, with interest on any part thereof at any time unpaid at the rate of  per cent per annum while Purchaser is not in default, and at the rate of ________ per cent per annum.</p>",
            "<h2>Agreements</h2>To execute and deliver to Purchaser or his (_____________) assigns, upon  payment in full of all sums owing hereon, less the amount then owing on any unpaid mortgage or mortgages, and the surrender of the duplicate of this contract, a good and sufficient warranty deed conveying title to the land, subject to abovementioned restrictions and easements and to any then unpaid mortgage or mortgages, but free from all other encumbrances, except such as may be herein set forth or shall have accrued or attached since the date hereof through the acts or omissions of persons other than Seller or his assigns.",
            "<h2>Parties</h2>To deliver to Purchaser as evidence of title, at Seller's option, either commitment for title insurance followed by a policy pursuant thereto insuring Purchaser or abstract of title covering the land, furnished by _________________________. The effective date of the policy or certification date of the abstract is to be approximately the date of this contract. Seller shall have the right to retain possession of such evidence of title during the life of this contract but upon demand shall lend it to Purchaser upon the pledging of a reasonable security.",
            "<h2>Liabilities</h2><ul><li>To purchase the land and pay Seller the sum aforesaid, with interest thereon as above provided. In addition, _________________________________</li><li>To use, maintain and occupy the land in accordance with any and all building and _______________________ and use restrictions applicable thereto.</li><li>To keep the land in accordance with all police, sanitary or other regulations imposed by any governmental or _______________________ authority.</li><li>To keep and maintain the land and the buildings and _____________________________ in as good condition as they are at the date hereof and not to commit waste, remove or demolish any improvements thereon, or otherwise diminish the value of Seller's security, without the written consent of Seller.</li>",
            "<h2>Amounts</h2>To pay monthly in addition to the monthly payment hereinbefore stipulated, the sum of ____________________(&euro; ) euros, which is an estimate of the monthly cost of taxes, special assessments, and insurance premiums for the land, which shall be credited by Seller on the unpaid principal balance owing on the contract. If Purchaser is not in default under the terms of this contract, Seller shall pay for Purchaser's account the taxes, special assessments and insurance premiums mentioned in Paragraph 2(e) above when due and before any penalty attaches, and submit receipts therefor to Purchaser upon demand.  The amounts so paid shall be added to the principal balance of this contract. The amount of the estimated monthly payment, under this paragraph, may be adjusted.",
  ];
  old_content = CKEDITOR.instances.editor1.getData();
  new_content= old_content + sections[v];
  CKEDITOR.instances.editor1.setData(new_content);

}


</script>
</body>
</html>
