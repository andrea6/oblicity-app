from django import forms
from django.forms import widgets
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from django.db.models import Q
from .models import UserProfile
from ckeditor.widgets import CKEditorWidget



class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Username'}), label="Username", max_length=100)
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Password'}), label="Password")


class SignUpForm(forms.Form):
    firstname = forms.CharField(widget=widgets.TextInput(attrs={'class':'form-control'}), label="First name", max_length=100)
    lastname = forms.CharField(widget=widgets.TextInput(attrs={'class':'form-control'}), label="Last name", max_length=100)
    username = forms.CharField(widget=widgets.TextInput(attrs={'class':'form-control'}), label="Username", max_length=100)
    password = forms.CharField(widget=widgets.PasswordInput(attrs={'class':'form-control'}), label="Password", max_length=100)
    email = forms.CharField(widget=widgets.EmailInput(attrs={'class':'form-control'}), label="Email", max_length=100)
    idcard = forms.CharField(widget=widgets.TextInput(attrs={'class':'form-control'}), label="ID Card", max_length=100)
    terms = forms.BooleanField(widget=widgets.CheckboxInput(), required=False)

    def clean_username(self):
        user =  self.cleaned_data['username']
        if User.objects.filter(username=user).exists():
            raise ValidationError(_("This username already exists. Please choose a different one."))
        return user

    def clean_email(self):
        email =  self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise ValidationError(_("This Email is already in use. Please use a different one to register a new account."))
        return email

    def clean_terms(self):
        terms = self.cleaned_data['terms']
        if not terms:
            raise ValidationError(_("You must accept the terms before using this service"))
        return terms

class PasswordResetForm(forms.Form):
    username = forms.CharField(widget=widgets.TextInput(attrs={'class':'form-control'}), label="Username", max_length=100)
    email = forms.CharField(widget=widgets.EmailInput(attrs={'class':'form-control'}), label="Email", max_length=100)

    def clean(self):
        cleaned_data = super(PasswordResetForm, self).clean()

        user = cleaned_data.get("username")
        email = cleaned_data.get("email")
        if user and email:
            if not User.objects.filter(username=user).exists() or not User.objects.filter(email=email).exists() or User.objects.get(username=user)!=User.objects.get(email=email):
                raise forms.ValidationError("Please check username/email pair.")


class ProfileUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProfileUpdateForm, self).__init__(*args,**kwargs)
        for name,field in self.fields.items():
            if 'class' in field.widget.attrs:
                field.widget.attrs['class'] += 'form-control'
            else:
                field.widget.attrs.update({'class': 'form-control'})

        self.fields['idcard'].widget.attrs.update({'readonly' : 'readonly'})
        self.fields['profession'].widget.attrs.update({'placeholder' : 'Profession'})
        self.fields['phone'].widget.attrs.update({'placeholder' : 'Phone'})
        self.fields['address'].widget.attrs.update({'placeholder' : 'Address'})
        self.fields['website'].widget.attrs.update({'placeholder' : 'Website'})
        self.fields['description'].widget.attrs.update({'placeholder' : 'Brief profile description'})
        self.fields['birth'].widget.attrs.update({'placeholder' : 'Birth date'})


    class Meta:
        model = UserProfile
        fields = [ 'profession', 'phone',  'address', 'idcard', 'website', 'description', 'picture', 'birth' ]





