# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0005_auto_20150514_0808'),
        ('profiles', '0006_userprofile_birth'),
    ]

    operations = [
    ]
