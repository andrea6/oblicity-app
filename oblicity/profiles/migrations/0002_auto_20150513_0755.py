# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='owner',
            field=models.ForeignKey(help_text='User associated to this profile', to=settings.AUTH_USER_MODEL, verbose_name='Owner'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='picture',
            field=sorl.thumbnail.fields.ImageField(default='/home/andrea/Desktop/repos/oblicity-app/oblicity/assets/img/profile-blank.png', verbose_name='Profile picture', upload_to='', help_text='Profile picture'),
        ),
    ]
