# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_auto_20150513_0755'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='picture',
            field=sorl.thumbnail.fields.ImageField(verbose_name='Profile picture', help_text='Profile picture', upload_to='', default='/static/img/profile-blank.png'),
        ),
    ]
