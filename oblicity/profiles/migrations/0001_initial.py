# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('profession', models.CharField(null=True, help_text='Profession', blank=True, max_length=64, verbose_name='Profession')),
                ('phone', models.CharField(null=True, help_text='Phone', blank=True, max_length=64, verbose_name='Phone')),
                ('address', models.CharField(null=True, help_text='Address', blank=True, max_length=64, verbose_name='Address')),
                ('idcard', models.CharField(null=True, help_text='ID Number', blank=True, max_length=64, verbose_name='ID Card Number')),
                ('website', models.URLField(null=True, help_text='Website', blank=True, verbose_name='Website')),
                ('description', models.CharField(null=True, help_text='Description', blank=True, max_length=512, verbose_name='Brief personal description')),
                ('picture', sorl.thumbnail.fields.ImageField(help_text='Profile picture', upload_to='', verbose_name='Profile picture')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, verbose_name='Owner', help_text='Content author. This field is visible only to admins')),
            ],
            options={
                'verbose_name_plural': 'User Profiles',
                'verbose_name': 'User Profile',
            },
        ),
    ]
