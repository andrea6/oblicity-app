# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0005_auto_20150517_1243'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='birth',
            field=models.DateField(verbose_name='Birth date', null=True, blank=True, help_text='Birth date'),
        ),
    ]
