from django.db import models
from model_utils.models import TimeStampedModel
from sorl.thumbnail import ImageField, get_thumbnail
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
import os
from django.conf import settings

class UserProfile(TimeStampedModel):
    class Meta:
            verbose_name = _("User Profile")
            verbose_name_plural = _("User Profiles")

    owner = models.ForeignKey(User, help_text=("User associated to this profile"), verbose_name=_("Owner"))
    birth = models.DateField(verbose_name=_("Birth date"), help_text=_("Birth date"), null=True, blank=True)
    profession = models.CharField(max_length=64, help_text=_("Profession"), verbose_name=_("Profession"), null=True, blank=True)
    phone = models.CharField(max_length=64, help_text=_("Phone"), verbose_name=_("Phone"), null=True, blank=True)
    address = models.CharField(max_length=64, help_text=_("Address"), verbose_name=_("Address"), null=True, blank=True)
    idcard = models.CharField(max_length=64, help_text=_("ID Number"), verbose_name=_("ID Card Number"), null=True, blank=True)
    website = models.URLField(help_text=_("Website"), verbose_name=_("Website"), null=True, blank=True)
    description = models.TextField(max_length=512, help_text=_("Description"), verbose_name=_("Brief personal description"), null=True, blank=True)
    picture = ImageField(help_text=_("Profile picture"), upload_to="profilepictures", verbose_name=_("Profile picture")  )


    def __str__(self):
        return self.owner.username+ _("'s profile")
