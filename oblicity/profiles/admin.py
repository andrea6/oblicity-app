from django.contrib import admin
from .models import UserProfile
from sorl.thumbnail.admin import AdminImageMixin
# Register your models here.

class UserProfileAdmin(AdminImageMixin, admin.ModelAdmin):

    def get_readonly_fields(self, request, obj=None):
        return ['owner',]



admin.site.register(UserProfile, UserProfileAdmin)
