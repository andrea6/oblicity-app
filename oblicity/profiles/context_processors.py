from django.utils.translation import ugettext as _
from .models import UserProfile

def user_profile(request):
    if not request.user.is_authenticated():
        return {}
    profile = UserProfile.objects.filter(owner=request.user).first()
    if profile:
        return {'profile':profile}
    else:
        return {}


