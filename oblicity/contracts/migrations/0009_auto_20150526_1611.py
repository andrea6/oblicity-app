# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contracts', '0008_contract_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='contract',
            name='last_editor',
            field=models.ForeignKey(verbose_name='Last edited by', help_text='The user who last edited this contract', default=1, related_name='last_contract', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contract',
            name='valid_from',
            field=models.DateField(verbose_name='Valid from', help_text='FROM date of validity for this contract'),
        ),
        migrations.AlterField(
            model_name='contract',
            name='valid_to',
            field=models.DateField(verbose_name='Valid to', help_text='TO date of validity for this contract'),
        ),
    ]
