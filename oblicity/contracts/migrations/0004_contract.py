# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import model_utils.fields
import django.utils.timezone
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0007_merge'),
        ('contracts', '0003_auto_20150519_0748'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contract',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(editable=False, verbose_name='created', default=django.utils.timezone.now)),
                ('modified', model_utils.fields.AutoLastModifiedField(editable=False, verbose_name='modified', default=django.utils.timezone.now)),
                ('name', models.CharField(unique=True, verbose_name='Template name', help_text='Template name', max_length=64)),
                ('filled_template', ckeditor.fields.RichTextField(verbose_name='Unedited body', help_text='The template filled with content, before editing')),
                ('edited_template', ckeditor.fields.RichTextField(verbose_name='Edited body', help_text='Body of the text as to last edit.')),
                ('valid_from', models.DateField(verbose_name='Valid from', help_text='FROM date of validity for this contract')),
                ('valid_to', models.DateField(verbose_name='Valid to', help_text='TO date of validity for this contract')),
                ('owners', models.ManyToManyField(verbose_name='Owners', help_text='Owners of this contract, can make modifications.', to='profiles.UserProfile')),
                ('template', models.ForeignKey(verbose_name='Source template', to='contracts.ContractTemplate', help_text='The template this contract is based on')),
            ],
            options={
                'verbose_name': 'Contract',
                'verbose_name_plural': 'Contracts',
            },
        ),
    ]
