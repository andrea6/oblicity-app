# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contracts', '0006_auto_20150524_1535'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contract',
            name='owners',
            field=models.ManyToManyField(help_text='Owners of this contract, can make modifications.', verbose_name='Owners', to=settings.AUTH_USER_MODEL),
        ),
    ]
