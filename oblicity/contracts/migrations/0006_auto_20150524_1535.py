# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contracts', '0005_auto_20150524_1524'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contract',
            name='valid_from',
            field=models.DateField(verbose_name='Valid from', auto_now_add=True, help_text='FROM date of validity for this contract'),
        ),
        migrations.AlterField(
            model_name='contract',
            name='valid_to',
            field=models.DateField(verbose_name='Valid to', auto_now_add=True, help_text='TO date of validity for this contract'),
        ),
    ]
