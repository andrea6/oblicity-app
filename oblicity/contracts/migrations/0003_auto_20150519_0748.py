# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contracts', '0002_auto_20150517_1535'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contracttemplate',
            name='name',
            field=models.CharField(unique=True, help_text='Template name', max_length=64, verbose_name='Template name'),
        ),
    ]
