# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contracts', '0004_contract'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contract',
            name='name',
            field=models.CharField(verbose_name='Template name', help_text='Template name', max_length=64),
        ),
    ]
