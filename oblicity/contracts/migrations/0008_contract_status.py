# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contracts', '0007_auto_20150524_1643'),
    ]

    operations = [
        migrations.AddField(
            model_name='contract',
            name='status',
            field=models.CharField(max_length=32, default='draft', choices=[('draft', 'Draft'), ('pending', 'Pending'), ('future', 'Future'), ('private', 'Private'), ('published', 'Published')]),
        ),
    ]
