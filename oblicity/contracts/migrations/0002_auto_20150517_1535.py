# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('contracts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='contracttemplate',
            name='content',
            field=ckeditor.fields.RichTextField(help_text='Template content', default='', verbose_name='Template content'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contracttemplate',
            name='name',
            field=models.CharField(verbose_name='Template name', help_text='Template name', max_length=64, default=''),
            preserve_default=False,
        ),
    ]
