# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ContractTemplate',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=64, help_text='Template name', null=True, verbose_name='Template name', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Contract Templates',
                'verbose_name': 'Contract Template',
            },
        ),
    ]
