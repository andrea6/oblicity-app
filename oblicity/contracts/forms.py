from django import forms
from ckeditor.widgets import CKEditorWidget
from ckeditor.fields import RichTextField
from django.utils.translation import ugettext as _
from .models import ContractTemplate, Contract
import autocomplete_light
from django.contrib.auth.models import User



class ContractWizard1(forms.Form):
    template = forms.ChoiceField(choices=[],label=_("Template:"))

    def __init__(self, *args, **kwargs):
        super(ContractWizard1, self).__init__(*args, **kwargs)
        self.fields['template'].choices= [(x.pk, x.name) for x in ContractTemplate.objects.all()]

class ContractWizard2(forms.Form):
    body = forms.CharField(widget=forms.widgets.HiddenInput())


class ContractWizard3(forms.Form):
    body_edited = forms.CharField(widget=CKEditorWidget(attrs={'class':'form-control'}))

class ContractWizard4(forms.Form):
    #owners = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Owners'}), label=_("Contract owners"))
    title = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Title'}), label=_("Contract title:"))
    status = forms.ChoiceField(choices=Contract.STATUS_CHOICES, label=_("Status:"))
    owners = autocomplete_light.MultipleChoiceField('UserProfileAutocomplete', label=_("Additional owners:"), required=False)
    notify_owners = forms.BooleanField(widget=forms.widgets.CheckboxInput(), label=_("Notify owners upon creation"),required=False, initial=True)
    valid_from = forms.DateField(widget=forms.widgets.DateInput(), label=_("Valid from:"))
    valid_to = forms.DateField(widget=forms.widgets.DateInput(), label=_("Valid to:"))


class EditContractForm(forms.ModelForm):
    class Meta:
        model = Contract
        fields = ['name', 'status', 'edited_template', 'valid_from', 'valid_to' ]

