from django.db import models
from django.utils.translation import ugettext as _
from ckeditor.fields import RichTextField
from model_utils.models import TimeStampedModel
from django.contrib.auth.models import User






class ContractTemplate(models.Model):
    class Meta:
            verbose_name = _("Contract Template")
            verbose_name_plural = _("Contract Templates")

    name = models.CharField(max_length=64, help_text=_("Template name"), verbose_name=_("Template name"),unique=True )
    content = RichTextField(help_text=_("Template content"), verbose_name=_("Template content"))

    def __str__(self):
        return self.name

    def get_content(self):
        return self.content

class Contract(TimeStampedModel):

    DRAFT = 'draft'
    PENDING = 'pending'
    FUTURE = 'future'
    PRIVATE = 'private'
    PUBLISHED = 'published'

    STATUS_CHOICES = (
        (DRAFT, _("Draft")),
        (PENDING, _("Pending")),
        (FUTURE, _("Future")),
        (PRIVATE, _("Private")),
        (PUBLISHED, _("Published")),
            )

    class Meta:
        verbose_name = _("Contract")
        verbose_name_plural = _("Contracts")

    def __str__(self):
        return self.name

    name = models.CharField(max_length=64, help_text=_("Template name"), verbose_name=_("Template name"))
    status = models.CharField(max_length=32, choices=STATUS_CHOICES, default=DRAFT)
    template = models.ForeignKey(to=ContractTemplate, help_text = _("The template this contract is based on"), verbose_name = _("Source template"))
    filled_template = RichTextField(help_text = _("The template filled with content, before editing"), verbose_name = _("Unedited body"))
    edited_template = RichTextField(help_text = _("Body of the text as to last edit."), verbose_name = _("Edited body"))
    owners = models.ManyToManyField(User, help_text = _("Owners of this contract, can make modifications."), verbose_name = _("Owners"))
    valid_from = models.DateField(help_text = _("FROM date of validity for this contract"), verbose_name = _("Valid from") )
    valid_to = models.DateField(help_text = _("TO date of validity for this contract"), verbose_name = _("Valid to"))
    last_editor = models.ForeignKey(User, help_text=_("The user who last edited this contract"), verbose_name=_("Last edited by"), related_name="last_contract")

