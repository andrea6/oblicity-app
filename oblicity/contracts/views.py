
from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect, QueryDict, JsonResponse
from django.views.generic import View
from django.views.generic.base import TemplateView
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.contrib.auth.decorators import login_required
import logging
from django.template import RequestContext
from django.conf import settings
from formtools.wizard.views import SessionWizardView
from .models import ContractTemplate, Contract
from django.shortcuts import get_object_or_404
import datetime
from profiles.models import UserProfile
from django.views.decorators.http import require_POST
from .forms import ContractWizard3,ContractWizard4, ContractWizard1, ContractWizard2, EditContractForm





logger = logging.getLogger('django')

WIZARD_STEP_0 = '0'
WIZARD_STEP_1 = '1'
WIZARD_STEP_2 = '2'
WIZARD_STEP_3 = '3'
WIZARD_STEP_4 = '4'

CONTRACT_WIZARD_TEMPLATES = { "0" : "contracts/contractwizard/new_contract_0.html",
                             "1" : "contracts/contractwizard/new_contract_1.html",
                             "2" : "contracts/contractwizard/new_contract_2.html",
                             "3" : "contracts/contractwizard/new_contract_3.html",
                             }

# Wizard for creating new contracts
class ContractWizard(SessionWizardView):
    form_list = [ContractWizard1, ContractWizard2, ContractWizard3, ContractWizard4]

    def get_template_names(self):
        return [CONTRACT_WIZARD_TEMPLATES[self.steps.current]]

    def done(self, form_list, **kwargs):
        logger.info(self.get_cleaned_data_for_step(WIZARD_STEP_3)['owners'] )
        contract = Contract()
        contract.name = self.get_cleaned_data_for_step(WIZARD_STEP_3)['title']
        contract.template = ContractTemplate.objects.get(pk=self.get_cleaned_data_for_step(WIZARD_STEP_0)['template'])
        contract.filled_template = self.get_cleaned_data_for_step(WIZARD_STEP_1)['body']
        contract.edited_template =  self.get_cleaned_data_for_step(WIZARD_STEP_2)['body_edited']
        contract.status = self.get_cleaned_data_for_step(WIZARD_STEP_3)['status']
        contract.valid_from = contract.valid_to = datetime.date.today()
        contract.last_editor = self.request.user
        contract.save()
        contract.owners.add(self.request.user)
        for profile_id in self.get_cleaned_data_for_step(WIZARD_STEP_3)['owners']:
            prof = UserProfile.objects.get(pk=profile_id)
            if not prof.owner in contract.owners.all():
                contract.owners.add(prof.owner)

        # contract.owners = []
        contract.valid_from = self.get_cleaned_data_for_step(WIZARD_STEP_3)['valid_from']
        contract.valid_to = self.get_cleaned_data_for_step(WIZARD_STEP_3)['valid_to']
        contract.save()
        messages.info(self.request, _("The contract has been modified successfully."))
        return redirect('assets_list')

    # Returns template and other information as context variables available on
    # the views, in order to correctly render templates
    def get_context_data(self, form, **kwargs):
        context = super(ContractWizard, self).get_context_data(form=form, **kwargs)
        if self.steps.current == WIZARD_STEP_1:
            template = ContractTemplate.objects.get(pk=self.get_cleaned_data_for_step(self.steps.prev)['template'])
            logger.info(self.get_cleaned_data_for_step(self.steps.prev)['template'])
            context.update({'template_content': template.get_content() })
        return context

    def get_form_initial(self, step):
        if step == WIZARD_STEP_2:
            return { 'body_edited' : self.get_cleaned_data_for_step(WIZARD_STEP_1)['body'] }

        if step == WIZARD_STEP_3:
            return { 'valid_from' : datetime.date.today().strftime("%m/%d/%Y")  }
        return self.initial_dict.get(step, {})



    # Overrides the class default method to inject data at certain steps
    def get_form(self, step=None, data=None, files=None):
        form = super(ContractWizard, self).get_form(step, data, files)

        if step is None:
            step = self.steps.current

        return form



def process_form_data(form_list):
    form_data = [form.cleaned_data for form in form_list]
    return form_data


# this view returns the raw content of a template, specified by the id GET
# parameter
def get_template_content(request,id):
    template = get_object_or_404(ContractTemplate,pk=id)
    return HttpResponse(template.get_content())

def get_edited_content(request,id):
    contract = get_object_or_404(Contract, pk=id)
    if not request.user in contract.owners.all():
        response = HttpResponse
        response.status_code = 401
        response.content =  _("Only owners of a contract may view its content")
        return response
    return HttpResponse(contract.edited_template)

# Delete contract is a POST only method
@require_POST
def delete_contract(request,id):
    response = HttpResponse()
    contract = get_object_or_404(Contract,pk=id)
    if request.user not in contract.owners.all():
        response.status_code = 401
        response.content = _("A user must be the owner of a contract in order to delete it")
        return response

    contract.owners.remove(request.user)
    response.status_code = 200
    messages.success( request, _("The contract "+contract.name+" has been successfully deleted"))
    return response


def edit_contract(request, id):

    contract = get_object_or_404(Contract, pk=id)
    logger.info(contract.owners)

    # If the method is POST, it received a form submission
    if request.method == 'POST':
        form = EditContractForm(request.POST, instance=contract)
        if form.is_valid():
            form.instance.last_editor = request.user
            form.save()
            logger.info(request.user.username+" successfully edited contract ID"+ str(contract.id))
            messages.success(request, "The contract "+contract.name+" was successfully saved.")
            return redirect("assets_list")
    else:
        form = EditContractForm(instance=contract)

    return render_to_response('contracts/edit_contract.html', {'form': form }, context_instance = RequestContext(request))
