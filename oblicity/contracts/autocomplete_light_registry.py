import autocomplete_light
from profiles.models import UserProfile



class UserProfileAutocomplete(autocomplete_light.AutocompleteModelTemplate):
    choice_template = "contracts/contractwizard/autocomplete/user_choice.html",
    model = UserProfile
    search_fields=['^owner__username', '^owner__email' ]
    attrs = {
        'placeholder': 'Owners usernames or emails',
        'data-autocomplete-minimum-characters': 1,
    }
    widget_attrs = {
        'data-widget-maximum-values' : 5,
    }


autocomplete_light.register( UserProfile, UserProfileAutocomplete)



