from django.contrib import admin
from .models import ContractTemplate, Contract


admin.site.register(ContractTemplate)
admin.site.register(Contract)
