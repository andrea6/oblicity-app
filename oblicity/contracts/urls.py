try:
    from django.conf.urls import *
except ImportError:  # django < 1.4
        from django.conf.urls.defaults import *

from . import views
from .views import ContractWizard

urlpatterns = patterns('',
                       url(r'^new/$', ContractWizard.as_view(), name='new_contract'),
                       url(r'^(?P<id>[0-9]+)/raw_template', views.get_template_content, name='get_template' ),
                       url(r'^(?P<id>[0-9]+)/edited_content', views.get_edited_content, name='get_contract_body' ),
                       url(r'^(?P<id>[0-9]+)/delete', views.delete_contract, name='delete_contract' ),
                       url(r'^(?P<id>[0-9]+)/edit', views.edit_contract, name='edit_contract' ),
                       )
