from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect, QueryDict, JsonResponse
from django.views.generic import View
from django.views.generic.base import TemplateView
from profiles.forms import LoginForm, SignUpForm, PasswordResetForm, ProfileUpdateForm
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.contrib.auth.decorators import login_required
import logging
from django.template import RequestContext
from profiles.models import UserProfile
import os
from django.conf import settings
from formtools.wizard.views import SessionWizardView

from contracts.models import Contract, ContractTemplate


# Imports the logger and makes it available to all views
logger = logging.getLogger('django')






# SignIn is implemented as Class Based View because, why not
class SignIn(View):
    form_class = LoginForm
    template_name = "frontend/signin.html"

    def get(self, request):
        form  = self.form_class()
        return render(request, self.template_name, {'form':form})

    def post(self, request):
        form = LoginForm(request.POST)
        # If the login form validates correctly
        if form.is_valid():
            usr = form.cleaned_data['username']
            pwd = form.cleaned_data['password']
            # Attempts autentication
            user = authenticate(username=usr,
                                password=pwd)
            # If the user is authenticated using the supplied credentials
            if user:
                if user.is_active:
                    login(request, user)
                    if request.GET.get('next',None):
                        return redirect(request.GET.get('next'))
                    # In case the signin route is called without a 'next' GET
                    # parameter set, it automatically redirects to the profile
                    # view
                    return redirect('profile')
                else:
                    logger.error(usr+" was deactivated and rejected for login.")
                    messages.error(request,_("""This user has been deactivated.
                                             Please contact an administrator."""))
            else:
                logger.error(usr +" was not found and rejected for login")
                messages.error(request,_("Incorrect username/password please try again."))

        return render(request, self.template_name, {'form': form})


class SignUp(View):
    form_class = SignUpForm
    template_name = "frontend/signup.html"

    def get(self, request):
        form = self.form_class()
        return render(request, self.template_name, {'form':form})

    def post(self, request):
        form = SignUpForm(request.POST)
        # The signup form was filled in correctly
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            firstname = form.cleaned_data["firstname"]
            lastname = form.cleaned_data["lastname"]
            email = form.cleaned_data["email"]
            idcard = form.cleaned_data["idcard"]
            # Creates a new user and fills in name information
            new_user = User.objects.create_user(username=username, email=email, password=password)
            new_user.first_name = firstname
            new_user.last_name = lastname
            new_user.save()
            # Creates an associated profile and fills in information
            new_profile = UserProfile()
            new_profile.owner = new_user
            new_profile.idcard = idcard
            new_profile.save()
            logger.info("User "+username+" was created successfully. User ID:"+str(new_user.id)+". Profile ID: "+str(new_profile.id))
            messages.success(request,_("You account was created successfully. You can now log in using the chosen username and password."))
            return redirect('signin')

        return render(request, self.template_name, {'form': form })


@login_required()
def profile(request):
    profile = UserProfile.objects.get(owner=request.user)
    if request.method == "POST":
        form = ProfileUpdateForm(request.POST, request.FILES, instance=profile)
        form.instance.owner = request.user
        if form.is_valid():
            form.save()
            logger.info("User "+ request.user.username+" successfully updated his profile.")
            messages.success(request, _("Your profile information has been updated successfully."))
    else:
        form = ProfileUpdateForm(instance=profile)

    return render_to_response("frontend/main_profile.html", {'form': form}, context_instance=RequestContext(request))


def signup(request):
    return render_to_response("frontend/signup.html", { }, )

def reset_password(request):
    form = PasswordResetForm
    if request.method == "POST":
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            user = User.objects.get(username=username)
            new_password = User.objects.make_random_password()
            user.set_password(new_password)
            user.save()
            user.email_user(subject=_("Oblicity App - Password reset"), message="The password for the user "+username+
                            """ associated with this email address has been reset. Your new temporary password is """+ new_password + ". You have 3 days time to change it.")
            logger.info("Password reset for user "+username)
            messages.info(request,_("Password reset successful. We have Emailed you your new temporary password."))

    return render_to_response("frontend/reset_password.html", {'form': form  }, context_instance=RequestContext(request))

def signout(request):
    logger.info("User "+str(request.user)+" successfully logged out")
    logout(request)
    return redirect('signin')



def assets_list(request):
    contracts = Contract.objects.filter(owners__in=[request.user])
    return render_to_response('frontend/assets_list.html', { 'contracts':contracts }, context_instance=RequestContext(request) )
