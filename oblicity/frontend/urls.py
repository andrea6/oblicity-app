try:
    from django.conf.urls import *
except ImportError:  # django < 1.4
        from django.conf.urls.defaults import *

from . import views
from .views import SignIn, SignUp

urlpatterns = patterns('',
                       url(r'^signin/$', SignIn.as_view(), name='signin'),
                       url(r'^signup/$', SignUp.as_view(), name='signup'),
                       url(r'^signout/$', views.signout, name='signout'),
                       url(r'^resetpass/$', views.reset_password, name='resetpass'),
                       url(r'^profile/$', views.profile, name='profile'),
                        url(r'^contracts/', include('contracts.urls')),
                       url(r'^assets/$', views.assets_list, name='assets_list'),

                       )
