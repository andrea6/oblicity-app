<!DOCTYPE html>
<html>
<head>
<?php include('head_base.php');?>
</head>
<body>
<script> var boxtest = localStorage.getItem('boxed'); if (boxtest === 'true') {document.body.className+=' boxed-layout';} </script>

<!-- Start: Header -->
<?PHP include('topbar.php'); ?>
<!-- End: Header -->
<!-- Start: Main -->
<div id="main">
  <!-- Start: Sidebar -->
  <?php include('sidebar.php'); ?>
  <!-- End: Sidebar -->
  <!-- Start: Content -->
  <section id="content_wrapper">
    <div id="topbar">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active"><a href="privacy_main.php">Legal</a></li>
          <li class="crumb-icon"><a href="main.php"><span class="glyphicon glyphicon-home"></span></a></li>
          <li class="crumb-link"><a href="main.php">Home</a></li>
          <li class="crumb-trail">Legal</li>
        </ol>
      </div>
    </div>
    <div id="content">
        <div class="row">
                <?php include('privacy_content.php'); ?>
        </div>
    </div>
  </section>
  <!-- End: Content -->

  <!-- Start: Right Sidebar -->
    <?PHP include('sidebar_right.php'); ?>
  <!-- End: Right Sidebar -->
</div>
<!-- End: Main -->

<?php include('scripts_base.php'); ?>
</body>
</html>
