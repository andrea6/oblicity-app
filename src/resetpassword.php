﻿<!DOCTYPE html>
<html>
<head>
 <?PHP include('head_base.php'); ?>
</head>

<body class="minimal login-page">
<script> var boxtest = localStorage.getItem('boxed'); if (boxtest === 'true') {document.body.className+=' boxed-layout';} </script>

<!-- Start: Main -->
<div id="main">
  <div id="content">
    <div class="row">
      <div id="page-logo"> <img src="img/logos/logo-white.png" class="img-responsive" alt="logo"> </div>
    </div>
    <div class="row">
      <div class="panel-bg">
        <div class="panel">
          <div class="panel-heading"> <span class="panel-title"> <span class="fa fa-lock text-purple2"></span> Password reset </span> </div>
          <div class="panel-body">
            <p>This procedure allows you to reset the password for your account. You must provide both your username and associated Email address.</p>
            <p>On successful password reset, we will send you an Email with the new password, which must be changed within 3 days.</p>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon">Username </span>
                <input type="text" class="form-control" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon">Email address </span>
                <input type="text" class="form-control" placeholder="">
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <a class="btn btn-sm bg-purple2 pull-right" href="#">Reset password</a>
            <span class="text-muted fs12">
                <a href="login.php"> Go back</a>
            </span>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="fixed-footer">
   <?PHP include('footer_notes.php'); ?>
</div>
<!-- End: Main -->

<div class="overlay-black"></div>

<?php include('scripts_base.php'); ?>
<script type="text/javascript">
jQuery(document).ready(function () {
	 // Init Full Page BG(Backstretch) plugin
  	 $.backstretch("img/background.jpg");
});
</script>
</body>
</html>
