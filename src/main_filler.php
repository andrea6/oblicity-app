<div id="content" class="prn">
      <div class="row">
        <div class="col-md-3 pl25 pr25 overflow-h">
          <h5 class="text-muted"> Account </h5>
          <hr class="short">
          <p> Lorem ipsum dolor sit amet, consectet adipcing elit. Nuncasta convallis lacus nec magna interdum, et rutrum nibh tristique nuncasta convallis. </p>
          <h5 class="mt30 text-muted"> Statistics </h5>
          <hr class="short">
          <p class="fs12 text-muted"> <span class="glyphicons glyphicons-user mr10 text-purple"></span> 122 Connections</p>
          <p class="fs12 text-muted"> <span class="glyphicons glyphicons-user_add mr10 text-orange"></span> 3 Requests</p>
          <p class="fs12 text-muted"> <span class="glyphicons glyphicons-star mr10 text-blue2"></span> 74 Contracts </p>
          <h5 class="mt30 text-muted"> Purchased Bundles </h5>
          <hr class="short">
          <p class="mb5"><span class="label bg-purple mr10">Commercial</span><span class="label bg-purple mr10">Rent</span><span class="label bg-purple mr10">Filings</span><span class="label bg-purple mr10">Loans</span><span class="label bg-purple mr10">Proceedings</span></p>
          <p><span class="label bg-purple mr10">Divorces</span><span class="label bg-purple mr10">Testaments</span><span class="label bg-purple mr10">Extras</span></p>
          <h5 class="mt30 text-muted"> Contact Information</h5>
          <hr class="short">
          <p class="fs12 text-muted"> <span class="glyphicons glyphicons-phone mr10 text-grey2 fs14"></span> 858-422-2422 </p>
          <p class="fs12 text-muted"> <span class="glyphicons glyphicons-skype mr10 text-teal2"></span> SaraMichaels </p>
        </div>
        <div class="col-md-9 profile-tabs pl25">
          <div class="tab-block">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#profile1" data-toggle="tab"> Edit Profile</a></li>
              <li><a href="#profile2" data-toggle="tab">Notifications</a></li>
            </ul>
            <div class="tab-content border-right-none pn">
              <div id="profile1" class="tab-pane p30 active">
                <div class="row">
                  <div class="col-sm-6 pr15">
                    <h5 class="text-muted"> Basic Information </h5>
                    <hr class="short mb35"/>
                    <div class="form-group">
                      <div class="input-group mb10"> <span class="input-group-addon"><span class="glyphicons glyphicons-old_man"></span> </span>
                        <input class="form-control" type="text" value="hando.rand@gmail.com" disabled="disabled">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group mb10"> <span class="input-group-addon"><span class="glyphicons glyphicons-keys"></span> </span>
                        <input class="form-control" type="text" placeholder="Password">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group mb10"> <span class="input-group-addon"><span class="glyphicons glyphicons-message_flag"></span> </span>
                        <input class="form-control" type="text" value="hando@oblicity.com">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group mb10"> <span class="input-group-addon"><span class="glyphicons glyphicons-book_open"></span> </span>
                        <input class="form-control" type="text" value="Oblicity Inc.">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group mb10"> <span class="input-group-addon"><span class="glyphicons glyphicons-home"></span> </span>
                        <input class="form-control" type="text" value="Pärnu Mnt 22">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group mb10"> <span class="input-group-addon"><span class="glyphicons glyphicons-phone"></span> </span>
                        <input class="form-control" type="text" value="(+858) 422-2422">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group mb10"> <span class="input-group-addon"><span class="glyphicons glyphicons-globe"></span> </span>
                        <input class="form-control" type="text" value="http://www.oblicity.com">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 pl15">
                    <h5 class="text-muted"> User Description </h5>
                    <hr class="short mb35"/>
                    <p class="text-red"><span class="glyphicons glyphicons-envelope mr10"></span>Please select your email preferences.</p>
                    <div class="row mb15">
                      <div class="col-sm-6">
                        <div class="cBox cBox-inline">
                          <input type="checkbox" id="profileOption1" name="check" value="None">
                          <label for="profileOption1">Promotions</label>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="cBox cBox-inline">
                          <input type="checkbox" id="profileOption2" name="check" value="None">
                          <label for="profileOption2">Invoices</label>
                        </div>
                      </div>
                    </div>
                    <div class="row mb15">
                      <div class="col-sm-6">
                        <div class="cBox cBox-inline">
                          <input type="checkbox" id="profileOption3" name="check" value="None">
                          <label for="profileOption3">Request</label>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="cBox cBox-inline">
                          <input type="checkbox" id="profileOption4" name="check" value="None">
                          <label for="profileOption4">Messages</label>
                        </div>
                      </div>
                    </div>
                    <div class="row mb35">
                      <div class="col-sm-6">
                        <div class="cBox cBox-inline">
                          <input type="checkbox" id="profileOption5" name="check" value="None">
                          <label for="profileOption5">Alerts</label>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="cBox cBox-inline">
                          <input type="checkbox" id="profileOption6" name="check" value="None">
                          <label for="profileOption6">Tasks</label>
                        </div>
                      </div>
                    </div>
                    <p class="text-red"><span class="glyphicons glyphicons-user mr10"></span>Please tell other users about yourself.</p>
                    <div class="form-group mb20">
                      <textarea class="form-control" placeholder="Your Profile Description..." rows="6"></textarea>
                    </div>
                    <button class="btn btn-default pull-right" type="button">Update Profile</button>
                  </div>
                </div>
              </div>
              <?php include('notifications_content.php');?>

            </div>
          </div>
        </div>
      </div>
    </div>