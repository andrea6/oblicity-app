﻿<!DOCTYPE html>
<html>
<head>
 <?PHP include('head_base.php'); ?>
</head>

<body class="minimal login-page">
<script> var boxtest = localStorage.getItem('boxed'); if (boxtest === 'true') {document.body.className+=' boxed-layout';} </script>

<!-- Start: Main -->
<div id="main">
  <div id="content">
    <div class="row">
      <div id="page-logo"> <img src="img/logos/logo-white.png" class="img-responsive" alt="logo"> </div>
    </div>
    <div class="row">
      <div class="panel-bg animated" id="loginPanel">
        <div class="panel" >
          <div class="panel-heading"> <span class="panel-title"> <span class="glyphicon glyphicon-lock text-purple2"></span> Sign in </span><?php if(!isset($_GET['r'])) { ?>   <span class="panel-header-menu pull-right mr15 text-muted fs12"><a href="login.php?r">Not <b>Hando Rand?</b></a></span><?php } ?> </div>
          <div class="panel-body">
            <?php if(isset($_GET['r'])) { ?>
                <h2>Welcome!</h2>
            <?php } else { ?>
                <div class="login-avatar"> <img src="img/avatars/10.jpg" width="150" alt="avatar"> </div>
            <?php }  ?>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span> </span>
              <?php if(isset($_GET['r'])) { ?>
                <input type="text" id="usernameField" class="form-control" placeholder="Username" >
              <?php } else { ?>
                  <input type="text" id="usernameField" class="form-control" value="hando.rand@gmail.com" disabled="disabled" >
              <?php }  ?>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon"><span class="glyphicon glyphicon-link"></span> </span>
                <input type="password" id="passwordField" class="form-control" placeholder="Password">
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <a class="btn btn-sm bg-purple2 pull-right" href="#" id="loginButton"><i class="fa fa-sign-in"></i> Login</a>
            <span class="text-muted fs12">
                <a href="resetpassword.php"> Forgotten password?</a> <br>
                <a href="signup.php"> Don't have an account? Sign up free.</a>
            </span>

            <div class="clearfix"></div>
            <br>
            <p class="text-center" style="border-top: solid thin #D6D7DB;" ><b>or</b> sign in with</p>
            <span>
              <a class="btn bg-light3 border-light6" href="#" style="width:30%; margin:0px 5px 0px 5px;"><i class="fa fa-google"></i> Google</a>
              <a class="btn bg-light3 border-light6" href="#" style="width:30%; margin:0px 5px 0px 5px;"><i class="fa fa-facebook-square"></i> Facebook</a>
              <a class="btn bg-light3 border-light6" href="#" style="width:30%; margin:0px 5px 0px 5px;"><i class="fa fa-twitter-square"></i> Twitter</a>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End: Main -->

<div class="overlay-black"></div>

<?php include('scripts_base.php'); ?>
<script>
    $(function(){
      $("#loginButton").click(function(){

         if($("#usernameField").val()=="hando.rand@gmail.com" && $("#passwordField").val()=="oblicity") {
           window.location="main.php";
         }
         else {

           $("#loginPanel").addClass("shake");
           $("#loginPanel").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
             $("#loginPanel").removeClass("shake");
           });
         }
      });
    })
</script>
<script type="text/javascript">
jQuery(document).ready(function () {
	 // Init Full Page BG(Backstretch) plugin
  	 $.backstretch("img/background.jpg");
});
</script>

</body>
</html>
