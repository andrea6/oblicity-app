﻿<!DOCTYPE html>
<html>
<head>
 <?PHP include('head_base.php'); ?>
</head>

<body class="minimal login-page">
<script> var boxtest = localStorage.getItem('boxed'); if (boxtest === 'true') {document.body.className+=' boxed-layout';} </script>

<!-- Start: Main -->
<div id="main">
  <div id="content">
    <div class="row">
      <div id="page-logo"> <img src="img/logos/logo-white.png" class="img-responsive" alt="logo"> </div>
    </div>
    <div class="row">
      <div class="panel-bg">
        <div class="panel">
          <div class="panel-heading"> <span class="panel-title"> <span class="fa fa-user text-purple2"></span> Sign up </span> </div>
          <div class="panel-body">
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon">First name </span>
                <input type="text" class="form-control" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon">Last name </span>
                <input type="text" class="form-control" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon">Username </span>
                <input type="text" class="form-control" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon">Password </span>
                <input type="text" class="form-control" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon">Email address</span>
                <input type="text" class="form-control" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon">ID Card # </span>
                <input type="text" class="form-control" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="input-group-addon">Account plan </span>
                <select class="form-control">
                  <option>Basic (Free)</option>
                  <option>Business (9.99&euro;/month)</option>
                  <option>Corporate (39.99&euro;/month)</option>
                </select>
              </div>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox"> I accept the <a href="privacy.php">privacy policy</a> and <a href="privacy.php">customer agreement</a>
              </label>
            </div>



          </div>
          <div class="panel-footer">
            <a class="btn btn-sm bg-purple2 pull-right" href="#"><i class="fa fa-check-circle"></i> Create account</a>
            <span class="text-muted fs12">
                <a href="login.php"> Have an account already? Log in here.</a>
            </span>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="fixed-footer">
   <?PHP include('footer_notes.php'); ?>
</div>
<!-- End: Main -->

<div class="overlay-black"></div>

<?php include('scripts_base.php'); ?>
<script type="text/javascript">
jQuery(document).ready(function () {
	 // Init Full Page BG(Backstretch) plugin
  	 $.backstretch("img/background.jpg");
});
</script>
</body>
</html>
