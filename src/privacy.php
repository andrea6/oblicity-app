﻿<!DOCTYPE html>
<html>
<head>
 <?PHP include('head_base.php'); ?>
</head>

<body class="minimal login-page">
<script> var boxtest = localStorage.getItem('boxed'); if (boxtest === 'true') {document.body.className+=' boxed-layout';} </script>

<!-- Start: Main -->
<div id="main">
  <div id="content">
    <div class="row">
      <div id="page-logo"> <img src="img/logos/logo-white.png" class="img-responsive" alt="logo"> </div>
    </div>
    <div class="row">
      <?php include('privacy_content.php'); ?>
    </div>
  </div>
</div>
<!-- End: Main -->

<div class="overlay-black"></div>

<?php include('scripts_base.php'); ?>
<script type="text/javascript">
jQuery(document).ready(function () {
	 // Init Full Page BG(Backstretch) plugin
  	 $.backstretch("img/background.jpg");
});
</script>
</body>
</html>
