<header class="navbar navbar-fixed-top">
  <div class="navbar-branding"> <span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines"></span> <a class="navbar-brand" href="dashboard.html"><img src="img/logos/header-logo.png"></a> </div>
  <div class="navbar-left">
    <div class="navbar-divider"></div>
    <div id="language_menu">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <span class="flag-xs flag-us"></span> <span class="small va-m">EN</span> <i class="fa fa-angle-down"></i></a>
      <ul class="dropdown-menu" role="menu">
        <li><a href="javascript:void(0);"><span class="flag-xs flag-fr"></span> Fran&ccedil;ais</a></li>
        <li><a href="javascript:void(0);"><span class="flag-xs flag-de"></span> Deutsche</a></li>
        <li><a href="javascript:void(0);"><span class="flag-xs flag-es"></span> Espa&ntilde;ol</a></li>
      </ul>
    </div>
  </div>
  <div class="navbar-right">
    <div class="navbar-search">
      <input type="text" id="HeaderSearch" placeholder="Search..." value="Search...">
    </div>
    <div class="navbar-menus">
      <div class="btn-group" id="alert_menu">
        <button type="button" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicons glyphicons-bell"></span> <b>4</b> </button>
        <ul class="dropdown-menu media-list" role="menu">
          <li class="p15 pb10">
            <ul class="list-unstyled">
              <li><span class="glyphicons glyphicons-bell text-orange2 fs16 mr15"></span><a href="edit.php?id=352">Contract <b>#352</b></a> expires in <b>3</b> days</li>
              <li class="pt10"><span class="glyphicons glyphicons-paperclip text-teal2 fs16 mr15"></span><b>M.Vahteristo</b> added an <a href="#">attachment</a></li>
              <li class="pt10"><span class="glyphicons glyphicons-bell text-orange2 fs16 mr15"></span><a href="edit.php?id=355">Contract <b>#355</b></a> requires your action</li>
              <li class="pt10"><span class="glyphicons glyphicons-ok text-orange2 fs16 mr15"></span><a href="edit.php?id=168">Contract <b>#168</b></a> was accepted</li>
            </ul>
          </li>
        </ul>
      </div>

      <div class="btn-group" id="toggle_sidemenu_r">
        <button type="button"> <span class="glyphicons glyphicons-parents"></span> </button>
      </div>
    </div>
  </div>
</header>