<aside id="sidebar_left">
  <div class="user-info">
    <div class="media"> <a class="pull-left" href="#">
      <div class="media-object border border-purple br64 bw2 p2"> <img class="br64" src="img/avatars/10.jpg" alt="..."> </div>
      </a>
      <div class="mobile-link"> <span class="glyphicons glyphicons-show_big_thumbnails"></span> </div>
      <div class="media-body">
        <h5 class="media-heading mt5 mbn fw700 cursor">Hando Rand<span class="caret ml5"></span></h5>
        <div class="media-links fs11"><a href="#">Menu</a><i class="fa fa-circle text-muted fs3 p8 va-m"></i><a href="login.php">Sign Out</a></div>
      </div>
    </div>
  </div>
  <div class="user-divider"></div>
  <div class="user-menu">
    <div class="row text-center mb15">
      <div class="col-xs-4"> <a href="main.php"> <span class="glyphicons glyphicons-home fs22 text-black"></span>
        <h5 class="fs11">Home</h5>
        </a> </div>
      <div class="col-xs-4"> <a href="inbox.php?folder=Inbox" class="ajax-disable" > <span class="glyphicons glyphicons-inbox fs22 text-black"></span>
        <h5 class="fs11">Inbox</h5>
        </a> </div>
      <div class="col-xs-4"> <a href="notifications.php"> <span class="glyphicons glyphicons-bell fs22 text-black"></span>
        <h5 class="fs11">Alerts</h5>
        </a> </div>
    </div>
    <div class="row text-center">
      <div class="col-xs-4 text-center"> <a href="#"> <span class="glyphicons glyphicons-imac fs22 text-black"></span>
        <h5 class="fs11">Digidoc</h5>
        </a> </div>
      <div class="col-xs-4"> <a  href="#"> <span class="glyphicons glyphicons-settings fs22 text-black"></span>
        <h5 class="fs11">Settings</h5>
        </a> </div>
      <div class="col-xs-4"> <a class="ajax-disable"  href="privacy_main.php"> <span class="fa fa-legal fs22 text-black"></span>
        <h5 class="fs11">Legal</h5>
        </a> </div>
    </div>
  </div>
  <div class="sidebar-menu">
    <ul class="nav sidebar-nav">
      <li class="active"> <a class="accordion-toggle" href="#sideOne"><span class="fa fa-magic"></span><span class="sidebar-title">Procedures</span><span class="caret"></span></a>
        <ul id="sideOne" class="nav sub-nav">
          <li><a href="edit.php"><span class="fa fa-file-o"></span> New Contract Wizard</a></li>
          <li><a href="#"><span class="fa fa-credit-card"></span> Manage identities</a></li>
          <li><a class="ajax-disable" href="#"><span class="fa fa-quote-right"></span> Manage references</a></li>
        </ul>
      </li>
      <li class="active"> <a class="accordion-toggle" href="#sideTwo"><span class="fa fa-list"></span><span class="sidebar-title">Assets</span><span class="caret"></span></a>
        <ul id="sideTwo" class="nav sub-nav">
          <li><a href="assets.php"><span class="fa fa-file-text-o"></span> My contracts</a></li>
          <li><a href="#"><span class="fa fa-suitcase"></span> Identity portfolio</a></li>
          <li><a class="ajax-disable" href="#"><span class="fa fa-check-square-o"></span> Duties overview</a></li>
        </ul>
      </li>
      <li class="active"> <a href="messages.php"  class="accordion-toggle ajax-disable"><span class="glyphicons glyphicons-inbox"></span><span class="sidebar-title">Messages</span><span class="caret"></span></a>
        <ul id="messages" class="nav sub-nav pb10">
          <li>
            <div class="email-menu">
              <ul class="list-unstyled">
                <li class="active"> <a href="inbox.php?folder=Inbox">Inbox</a> <span class="pull-right">10</span></li>
                <li> <a class="ajax-disable" href="inbox.php?folder=Saved">Saved</a> <span class="pull-right">4</span></li>
                <li> <a class="ajax-disable" href="inbox.php?folder=Drafts">Drafts</a> <span class="pull-right">2</span></li>
                <li> <a class="ajax-disable" href="inbox.php?folder=Trash">Trash</a> <span class="pull-right">25</span></li>
                <li class="menu-header text-dark2 mt15"> <span class="glyphicons glyphicons-bell"></span> <a class="ajax-disable" href="inbox.php?folder=Inquiries">Inquiries</a> <span class="pull-right">6</span></li>
                <li class="menu-header text-dark2"> <span class="glyphicons glyphicons-envelope"></span> <a class="ajax-disable" href="inbox.php?folder=Requests">Action requests</a> <span class="pull-right">4</span></li>
              </ul>
              <div class="clearfix"></div>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</aside>