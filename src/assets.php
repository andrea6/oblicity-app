<!DOCTYPE html>
<html>
<head>
<?php include('head_base.php');?>
</head>
<body>
<script> var boxtest = localStorage.getItem('boxed'); if (boxtest === 'true') {document.body.className+=' boxed-layout';} </script>
<?php
$names = array("John Levemberg","Andrea Paoletti","Arnold J. Assoc.","Dr. Pablo Neruda","Mika Aalto", "Paavo Kulmala", "Lauri J&auml;rvi", "Sanna Lahti", "Kristina Lukkanen", "Mika Jokela", "Laura Takala", "Urmo Toivonen");
$subjects = array("Nunc vel ultricies urna, in elementum lacus. Pellentesque quis libero tortor", "Pellentesque laoreet neque a mi tincidunt tincidunt", "Sed eros urna, scelerisque eu quam in, pharetra euismod nisl", "Sed sollicitudin efficitur mi, ut tincidunt felis facilisis ac", "Morbi sit amet dolor in est viverra gravida", "Nam vitae ante at quam placerat ornare", "In gravida elementum elit id pretium. Ut scelerisque libero augue");
$filenames = array("doku_signed1.pdf","contract.pdf","nda.pdf","annexes.pdf","cv.pdf","certificates.pdf");
$tags = array("work", "important", "delay", "business" , "online");
$status = array("open","closed","canceled","updated", "renewed") ;
$types = array("Contract for services", "Car selling","House rental", "Personal loan", "Rent to own", "Land sale","Construction", "Waiver of liability", "Last will and testament","Partnership agreement");
$cnames = array("Contract", "Document", "Agreement_", "signed_doc_");
?>
<!-- Start: Header -->
<?PHP include('topbar.php'); ?>
<!-- End: Header --> 
<!-- Start: Main -->
<div id="main"> 
  <!-- Start: Sidebar -->
  <?php include('sidebar.php'); ?>
  <!-- End: Sidebar -->
  <!-- Start: Content -->
  <section id="content_wrapper">
    <div id="topbar">
      <div class="topbar-left">
        <ol class="breadcrumb">
          <li class="crumb-active"><a href="#">Assets</a></li>
          <li class="crumb-icon"><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
          <li class="crumb-link"><a href="main.php">Home</a></li>
          <li class="crumb-trail">Contracts</li>
        </ol>
      </div>
    </div>
    <div id="content">
       <div class="row">
        <div class="col-md-12">
          <div class="panel panel-visible">
            <div class="panel-heading">
              <div class="panel-title hidden-xs"> <span class="glyphicon glyphicon-tasks"></span> Editable Data Table</div>
            </div>
            <div class="panel-body pbn">
              <table class="table table-striped table-bordered table-hover" id="datatable">
                <thead>
                  <tr>
                    <th>Contract name</th>
                    <th class="hidden-xs">Owner</th>
                    <th class="visible-lg">Validity</th>
                    <th class="hidden-xs hidden-sm">Type</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th class="text-center hidden-xs">Actions</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  $l=rand(14,63);
                  for($c=0;$c<$l;$c++) {
                ?>
                  <tr>
                    <td><span class="xedit"><?php echo $cnames[rand(0,sizeof($cnames)-1)]  ?><?php echo rand(1,20); ?></span></td>
                    <td class="hidden-xs"><span class="xedit"><?php echo $names[rand(0,sizeof($names)-1)]  ?></span></td>

                    <td class="visible-lg"><span class="xedit" data-type="date"><?php echo rand(1,31); ?>/<?php echo rand(0,12); ?>/<?php echo rand(14,15); ?></span></td>
                    <td><?php echo $types[rand(0,sizeof($types)-1)]  ?></td>
                    <td><?php echo rand(1,150)*100; ?>&euro;</td>
                    <td class="hidden-xs hidden-sm"><span class="label bg-grey"><?php echo $status[rand(0,sizeof($status)-1)]  ?></span></td>

                    <td class="hidden-xs text-center"><div class="btn-group">
                        <button type="button" onclick="window.location='edit.php?id=<?php echo rand(4694,4566); ?>'" class="btn bg-grey2 btn-gradient btn-sm"> <span class="glyphicons glyphicons-pencil"></span> </button>
                        <button type="button" class="btn bg-black btn-gradient btn-sm dropdown-toggle" data-toggle="dropdown"> <span class="fa fa-ellipsis-v"></span> </button>
                        <button type="button" class="btn bg-red btn-gradient btn-sm" onclick="$(this).parent().parent().parent().hide(500)"> <span class="glyphicons glyphicons-bin"></span> </button>
                        <ul class="dropdown-menu checkbox-persist pull-right text-left" role="menu">
                          <li><a><i class="fa fa-user"></i> Sign </a></li>
                          <li><a><i class="fa fa-envelope-o"></i> Send to User.. </a></li>
                          <li><a><i class="fa fa-tag"></i> Sign digitally </a></li>
                          <li><a><i class="fa fa-file-text-o"></i> Duplicate </a></li>
                        </ul>
                      </div></td>
                  </tr>
                  <?php
                    }
                  ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End: Content -->

  <!-- Start: Right Sidebar -->
    <?PHP include('sidebar_right.php'); ?>
  <!-- End: Right Sidebar -->
</div>
<!-- End: Main --> 

<?php include('scripts_base.php');?>
<!-- Page Plugins -->
<script type="text/javascript" src="vendor/plugins/datatables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="vendor/plugins/datatables/js/datatables.js"></script><!-- Datatable Bootstrap Addon -->
<script type="text/javascript" src="vendor/plugins/datatables/extras/TableTools/media/js/TableTools.min.js"></script><!-- Datatable TableTools Addon -->
<script type="text/javascript" src="vendor/plugins/datatables/extras/TableTools/media/js/ZeroClipboard.js"></script><!-- Datatable TableTools Addon -->
<script type="text/javascript" src="vendor/editors/xeditable/js/bootstrap-editable.js"></script>
<script type="text/javascript" src="vendor/plugins/chosen/chosen.jquery.js"></script>

<script type="text/javascript">
jQuery(document).ready(function () {

	 "use strict";

       // Init Xeditable Plugin
  $.fn.editable.defaults.mode = 'popup';
  $('.xedit').editable();


  // Init Datatables with Tabletools Addon
  $('#datatable').dataTable( {
	"aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ -1 ] }],
	"oLanguage": { "oPaginate": {"sPrevious": "", "sNext": ""} },
	"iDisplayLength": 15,
	"aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
	"sDom": 'T<"panel-menu dt-panelmenu"lfr><"clearfix">tip',
	"oTableTools": {
		"sSwfPath": "vendor/plugins/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
	}
  });

  // Add Placeholder text to datatables filter bar
  $('.dataTables_filter input').attr("placeholder", "Enter Filter Terms Here....");

  // Manually Init Chosen on Datatables Filters
  $("select[name='datatable_length']").chosen();



});
</script>

</body>
</html>
