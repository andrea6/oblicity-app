<?php
  $names = array("John Levemberg","Andrea Paoletti","Arnold J. Assoc.","Dr. Pablo Neruda","Mika Aalto", "Paavo Kulmala", "Lauri J&auml;rvi", "Sanna Lahti", "Kristina Lukkanen", "Mika Jokela", "Laura Takala", "Urmo Toivonen");
  $subjects = array("Nunc vel ultricies urna, in elementum lacus. Pellentesque quis libero tortor", "Pellentesque laoreet neque a mi tincidunt tincidunt", "Sed eros urna, scelerisque eu quam in, pharetra euismod nisl", "Sed sollicitudin efficitur mi, ut tincidunt felis facilisis ac", "Morbi sit amet dolor in est viverra gravida", "Nam vitae ante at quam placerat ornare", "In gravida elementum elit id pretium. Ut scelerisque libero augue");
  $filenames = array("doku_signed1.pdf","contract.pdf","nda.pdf","annexes.pdf","cv.pdf","certificates.pdf");
  $tags = array("work", "important", "delay", "business" , "online");
  $status = array("open","closed","canceled","updated", "renewed") ;
  $types = array("Contract for services", "Car selling","House rental", "Personal loan", "Rent to own", "Land sale","Construction", "Waiver of liability", "Last will and testament","Partnership agreement");
  $cnames = array("Contract", "Document", "Agreement_", "signed_doc_");
?>


<div id="profile2" class="tab-pane p10">
                <table class="table table-widget table-striped table-checklist mt15" id="profile_tasks">
                  <thead>
                    <tr>
                      <th>Contract</th>
                      <th>Progress</th>
                      <th>Status</th>
                      <th>Notes</th>
                      <th>Deadline</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php for($c=0;$c<rand(9,17);$c++) { ?>
                    <tr>
                      <td class="text-slash"><?php echo $cnames[rand(0,sizeof($cnames)-1)]; ?><?php echo rand(3,12); ?> (Contract #<?php echo rand(454,867); ?>)</td>
                      <td><div class="progress">
                            <?php $val=rand(0,100); ?>
                          <div class="progress-bar progress-bar-purple" role="progressbar" aria-valuenow="<?php echo $val; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $val; ?>%"><span class="sr-only"><?php echo $val; ?>% Complete (success)</span> </div>
                        </div></td>
                      <td><span class="label bg-grey2"><?php echo $status[rand(0,sizeof($status)-1)]; ?></span></td>
                      <td class="text-slash text-muted"><small><?php echo substr($subjects[rand(0,sizeof($subjects)-1)],0,32); ?>..</small></td>
                      <td class="text-slash semi-bold"><?php echo rand(1,31); ?>/<?php echo rand(0,12); ?>/20<?php echo rand(14,15); ?></td>
                      <td class="text-right"><div class="cBox cBox-inline">
                          <input type="checkbox" id="tableBox" name="check" value="None"/>
                          <label for="tableBox"></label>
                        </div></td>
                    </tr>
                    <?PHP } ?>
                  </tbody>
                </table>
              </div>