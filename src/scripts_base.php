

<!-- Overlay  -->

<div style="position:fixed; width:100%; height:160px; left:0; bottom:0; background-color:rgba(255,255,255,0.6); color:#111; text-salign:center; box-shadow:0px -10px 30px rgba(0,0,0,0.2); border: solid thin rgba(0,0,0,0.3);">
     <p style="text-align:center;">All rights and code ownership reserved until payment is completed. This is a <b>confidential</b> preview reserved to the Client.</p>
     <h2 style="text-align:center;">OblicityApp Responsive GUI v1.168 - Tech Demo</h2>
     <p style="text-align:center;"><a href="http://www.recursive.ee"><img src="img/recursive.png" alt="" style="max-height:3.5em;" /></a></p>
     <h4 style="text-align:center;"><a href="agreement.pdf">project agreement</a></h4>


</div>


<!-- End Overlay  -->



<!-- Google Map API -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>

<!-- jQuery -->
<script type="text/javascript" src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script> <!-- Bootstrap -->
<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Page Plugins -->
<script type="text/javascript" src="vendor/plugins/backstretch/jquery.backstretch.min.js"></script>

<script type="text/javascript" src="vendor/editors/summernote/summernote.js"></script>

<!-- Theme Javascript -->
<script type="text/javascript" src="js/utility/spin.min.js"></script>
<script type="text/javascript" src="js/utility/underscore-min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript">
jQuery(document).ready(function () {

	 "use strict";
     // Init Theme Core
     Core.init();
});
</script>